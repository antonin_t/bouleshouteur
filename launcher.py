from tkinter import N

import random
import utils
from ball import LauncherBall


class Launcher:
    """
    Classe 'Launcher' :
        Gère le lanceur de balles, qui prend une apparence d'arbalète.
    """
    def __init__(self, app, canvas, border, colors):
        """
        Constructeur de la classe
        :param app: instance de la classe App
        :param canvas: instance du canvas
        :param border: instance de la classe Border
        :param colors: liste des couleurs
        """
        self.__x = 225
        self.__y = 500 + 20
        self.__width = 150
        self.__height = 150
        self.__canvas = canvas
        self.__rotationrate = 2.5
        self.__currentangle = 0
        self.__maxangle = 65
        self.__imagepath = "assets/textures/crossbow.png"
        self.__image = utils.loadimage(self.__imagepath, self.__width, self.__height, self.__currentangle)
        self.__launcher = self.__canvas.create_image(self.__x, self.__y, anchor=N, image=self.__image)
        self.__launchball = LauncherBall(app, self.__canvas, colors[random.randint(0, len(colors) - 1)],
                                         self.__currentangle, border, colors)

    def getrotationrate(self):
        """
        :return: Taux de rotation par mouvement du lanceur
        """
        return self.__rotationrate

    def getcurrentangle(self):
        """
        :return: Angle actuel
        """
        return self.__currentangle

    def getmaxangle(self):
        """
        :return: Angle maximal
        """
        return self.__maxangle

    def getlaunchball(self):
        """
        :return: instance de la classe LauncherBall
        """
        return self.__launchball

    def rotate(self, angle):
        """
        Permet de faire pivoter le lanceur
        :param angle: Angle du lanceur
        """
        if self.__launchball.canlaunchermove():
            self.__currentangle = angle
            self.__image = utils.loadimage(self.__imagepath, self.__width, self.__height, angle)
            self.__canvas.itemconfig(self.__launcher, image=self.__image)
            self.__launchball.launchermove(self.__currentangle)

    def launch(self):
        """
        Lance une balle
        """
        if not self.__launchball.ismoving():
            self.__launchball.launch()
