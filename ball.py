import random
import threading

from math import cos, sin, radians


def testcoordinates(balls, x, y):
    """
    Permet de tester si une balle rentre en collision avec une autre
    :param balls: Liste des balles
    :param x: Coordonnée X de la balle
    :param y: Coordonnée Y de la balle
    :return: 'True' si la balle ne rentre pas en collision, 'False' si l'inverse
    """
    for ballslist in list(reversed(balls)):
        for ball in ballslist:
            if ball is not None:
                if ball.getx() < x < (ball.getx() + Ball.width) and ball.gety() < y < (ball.gety() + Ball.height):
                    return False
    return True


def checkneighbors(balls, x, y, canvas, rmax):
    """
    Permet de vérifier les balles voisines ainsi que leur couleur
    :param balls: Liste des balles
    :param x: Coordonnée du tableau de balles X
    :param y: Coordonnée du tableau de balles Y
    :param canvas: instance du canvas
    :param rmax: nombre de récursion maximale
    :return: 'True' si la balle doit être supprimée, 'False' si non
    """
    isneighbors = False
    fneighbors = []
    if rmax <= 4:
        if balls[y][x] is not None:
            color = balls[y][x].getcolor()
            neighbors = [[y - 1, x], [y - 1, x + 1], [y + 1, x], [y + 1, x + 1], [y, x - 1], [y, x + 1]]
            for nb in neighbors:
                if nb[0] == 0 or nb[0] % 2 == 0:
                    d = 14
                else:
                    d = 13
                if nb[0] >= 0 and 0 <= nb[1] <= d:
                    if balls[nb[0]][nb[1]] is not None:
                        if balls[nb[0]][nb[1]].getcolor() == color:
                            fneighbors.append([nb[1], nb[0]])
                            isneighbors = True
            for ngb in fneighbors:
                checkneighbors(balls, ngb[0], ngb[1], canvas, rmax + 1)
                if balls[ngb[1]][ngb[0]] is not None:
                    canvas.delete(balls[ngb[1]][ngb[0]].getball())
                    balls[ngb[1]][ngb[0]] = None
    return isneighbors


def checkifend(balls, lball, canvas):
    """
    Permet de vérifier si le jeu est terminé
    :param balls: Liste des balles
    :param lball: Dernière balle lancée
    :param canvas: instance du canvas
    """
    ok = True
    for ballslist in balls:
        for ball in ballslist:
            if ball is not None:
                ok = False
    if ok:
        lball.__launchermovestate = False
        lball.__movingstate = True
        canvas.create_text(225, 315, fill="green",
                           text="Félicitations ! Vous avez gagné !")
        canvas.create_text(225, 335, fill="dark gray",
                           text="(& perdu quelques minutes de votre vie)")


class Ball:
    """
    Classe 'Ball' :
        Gère les balles dans le jeu
    """

    width = 30
    height = 30

    def __init__(self, canvas, color):
        """
        Constructeur de la classe
        :param canvas: instance du canvas
        :param color: liste des couleurs
        """
        self.__color = color
        self._x = 0
        self._y = 0
        self._width = Ball.width
        self._height = Ball.height
        self._canvas = canvas
        self._ball = None

    def setposition(self, x, y):
        """
        Permet d'indiquer la position
        :param x: Coordonnée X
        :param y: Coordonnée Y
        """
        self._x = x
        self._y = y

    def getx(self):
        """
        :return: Coordonnée X
        """
        return self._x

    def gety(self):
        """
        :return: Coordonnée Y
        """
        return self._y

    def getball(self):
        """
        :return: instance de la Ball Tkinter
        """
        return self._ball

    def setcolor(self, color):
        """
        Permet d'indiquer la couleur
        :param color: Couleur
        """
        self.__color = color

    def getcolor(self):
        """
        :return: Couleur
        """
        return self.__color

    def getcanvas(self):
        """
        :return: instance du canvas
        """
        return self._canvas

    def place(self):
        """
        Permet de placer la balle
        """
        x2 = self._x + self._width
        y2 = self._y + self._height
        self._ball = self._canvas.create_oval(self._x, self._y, x2, y2, fill=self.__color, outline="")


class LauncherBall(Ball):
    """
    Classe 'LauncherBall' :
        Gère la balle lancée, cette classe hérite de la classe 'Ball'
    """

    def __init__(self, balls, canvas, color, angle, border, colors):
        """
        Constructeur de la classe
        :param balls: Liste des balles
        :param canvas: instance du canvas
        :param color: Couleur
        :param angle: Angle du lanceur
        :param border: instance de la classe Border
        :param colors: liste des couleurs
        """
        super().__init__(canvas, color)
        self.__balls = balls
        self.__colors = colors
        self.__launcherangle = angle
        self.__originx = 209.5
        self.__originy = 576
        self.__border = border
        self.__angle = 0
        self.setposition(self.__originx, self.__originy)
        self.place()
        self.__movingstate = False
        self.__loadstate = 0
        self.__loadtimer = None
        self.__launchtimer = None
        self.__launchermovestate = True
        self.__continue = False
        self.load()

    def ismoving(self):
        """
        :return: 'True' si la balle est en mouvement
        """
        return self.__movingstate

    def canlaunchermove(self):
        """
        :return: 'True' si le lanceur peut bouger à nouveau
        """
        return self.__launchermovestate

    def __calculateangle(self, langle):
        """
        (Méthode privée)
        :param langle: Angle
        :return: Angle modifié
        """
        if self._x >= self.__border.getmaxx() or self._x <= self.__border.getminx():
            langle = 180 - langle
        if self._y >= self.__border.getmaxy() or self._y <= self.__border.getminy():
            langle = -langle
        return langle

    def load(self):
        """
        Chargement de la balle dans le lanceur
        """
        self.__angle = abs(180 - self.__launcherangle + 90) - 180
        self.setposition(self._x - cos(radians(self.__angle)), self._y - sin(radians(self.__angle)))
        self.move()
        self.__loadstate += 1
        self.__launchermovestate = False
        self.__movingstate = True
        self.__loadtimer = threading.Timer(0.02, self.load)
        self.__loadtimer.start()
        if self.__loadstate >= 50:
            self.__loadstate = 0
            self.__loadtimer.cancel()
            self.__movingstate = False
            self.__launchermovestate = True
            self.__continue = True

    def move(self):
        """
        Permet de déplacer la balle
        """
        x2 = self._x + self._width
        y2 = self._y + self._height
        self._canvas.coords(self._ball, self._x, self._y, x2, y2)
        self.__continue = testcoordinates(self.__balls, self.getx(), self.gety())
        if not self.__continue:
            tball = Ball(self._canvas, self.getcolor())

            y = int(self._y // Ball.width)
            x = int(self._x // Ball.width)

            if self.__balls[y][x] is not None:
                y += 1

            if y == 0 or y % 2 == 0:
                nn = 14
                d = 1.4
            else:
                nn = 13
                d = (Ball.width / 2) + 1.4

            self.__balls[y][x] = tball

            tball.setposition((x * Ball.width) + d, y * 30)
            self._canvas.delete(self.getball())
            tball.place()

            res = checkneighbors(self.__balls, x, y, self._canvas, 0)
            if res:
                if self.__balls[y][x] is not None:
                    self._canvas.delete(self.__balls[y][x].getball())
                    self.__balls[y][x] = None
                    checkifend(self.__balls, self, self._canvas)

    def reset(self):
        """
        Permet de remettre une balle dans le lanceur
        """
        self.setposition(self.__originx, self.__originy)
        self.setcolor(self.__colors[random.randint(0, len(self.__colors) - 1)])
        self.place()
        self.load()

    def launchermove(self, angle):
        """
        Permet de déplacer la balle selon le lanceur
        :param angle: Angle du lanceur
        """
        if not self.__movingstate:
            self.__launcherangle = angle
            self.__angle = abs(180 - self.__launcherangle + 90) - 180
            coef = 50
            self.setposition(self.__originx - (cos(radians(self.__angle)) * coef),
                             self.__originy - (sin(radians(self.__angle)) * coef))
            self.move()

    def launch(self):
        """
        Permet de lancer la balle
        """
        if self._x >= self.__border.getmaxx() or self._x <= self.__border.getminx():
            self.__angle = self.__calculateangle(self.__angle)
        if self._y >= self.__border.getmaxy() or self._y <= self.__border.getminy():
            self.__angle = self.__calculateangle(self.__angle)

        self.setposition(self._x - (cos(radians(self.__angle)) * 3.5), self._y - (sin(radians(self.__angle)) * 3.5))

        self.move()
        self.__movingstate = True
        self.__launchtimer = threading.Timer(0.02, self.launch)
        self.__launchtimer.start()
        if not self.__continue:
            self.__launchtimer.cancel()
            self.reset()
            self.__movingstate = False
