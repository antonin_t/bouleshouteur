from PIL import Image, ImageTk


def loadimage(path, width, height, angle):
    image = Image.open(path)
    image.thumbnail((width, height), Image.ANTIALIAS)
    if angle is None:
        return ImageTk.PhotoImage(image)
    else:
        return ImageTk.PhotoImage(image.rotate(angle))
