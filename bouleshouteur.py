import random
from tkinter import *

from ball import Ball
from border import Border
from launcher import Launcher


# Antonin : 30/05 15h10
# Alexis : 30/05 15h35
# Adam : 30/05 16h00


class App:
    """
    Classe 'App' :
        Classe principale de l'application / programme
    """

    def __init__(self, win):
        """
        Constructeur de la classe
        :param win: élément Tkinter
        """
        self.__win = win
        self.__win.title("BouleShouteur - v1.0")
        self.__win.resizable(width=FALSE, height=FALSE)
        self.__frame = Frame(self.__win, width=450, height=650, relief=RIDGE, borderwidth=12, background="black")
        self.__frame.grid()
        self.buildmenubar()
        self.__border = Border(1, 1, 420, 620)
        self.__gamebox = self.buildgamebox()
        self.__colors = ["red3", "orange", "green", "deep pink", "blue", "green1", "black"]
        self.__balls = []
        self.createball()
        self.__launcher = Launcher(self.__balls, self.__gamebox, self.__border, self.__colors)
        self.__win.bind("<KeyPress-Left>", self.leftkeypress)
        self.__win.bind("<KeyPress-Right>", self.rightkeypress)
        self.__win.bind("<KeyPress-space>", self.launchkeypress)

    def getborder(self):
        """
        :return: instance de la classe Border
        """
        return self.__border

    def buildmenubar(self):
        """
        Permet de créer la barre de menus
        """
        menubar = Menu(self.__win)
        menubar.add_command(label="Quitter", command=self.quit)
        self.__win.config(menu=menubar)

    def buildgamebox(self):
        """
        Permet de créer le canvas du jeu
        :return: instance du canvas
        """
        canvas = Canvas(self.__frame, width=450, height=650)
        canvas.pack()
        return canvas

    def leftkeypress(self, event):
        """
        Fonction déclenchée par l'appui sur la touche 'flèche gauche'
        """
        total = self.__launcher.getcurrentangle() + self.__launcher.getrotationrate()
        if total >= self.__launcher.getmaxangle() or total <= -self.__launcher.getmaxangle():
            return
        self.__launcher.rotate(total)

    def rightkeypress(self, event):
        """
        Fonction déclenchée par l'appui sur la touche 'flèche droite'
        """
        total = self.__launcher.getcurrentangle() - self.__launcher.getrotationrate()
        if total >= self.__launcher.getmaxangle() or total <= -self.__launcher.getmaxangle():
            return
        self.__launcher.rotate(total)

    def launchkeypress(self, event):
        """
        Fonction déclenchée par l'appui sur la touche 'espace'
        """
        self.__launcher.launch()

    def createball(self):
        """
        Permet de générer la liste des balles via la fonction ci-dessous
        """
        nlines = 11
        for i in range(0, 50, 1):
            if i == 0 or i % 2 == 0:
                n = True
            else:
                n = False
            if i < nlines:
                line = self.generateballsline(n, (i * 30) + 1, True)
            else:
                line = self.generateballsline(n, (i * 30) + 1, False)
            self.__balls.append(line)

    def generateballsline(self, n, h, b):
        """
        Permet de générer une ligne de balles
        :param n: Si 'True' alors 15 balles par ligne, sinon 14
        :param h: hauteur (coordonnée Y)
        :param b: Si 'True' crée une ligne de balles, sinon, crée une ligne vide
        :return: la ligne de balles
        """
        line = []
        if n:
            nn = 15
            d = 1.4
        else:
            nn = 14
            d = (Ball.height / 2) + 1.4
        for i in range(0, nn, 1):
            if b:
                ball = Ball(self.__gamebox, self.__colors[random.randint(0, len(self.__colors) - 1)])
                ball.setposition((i * ball.width) + d, h)
                ball.place()
                line.append(ball)
            else:
                line.append(None)

        return line

    def getballs(self):
        """
        :return: liste des balles
        """
        return self.__balls

    def quit(self):
        """
        Permet de quitter proprement le programme
        """
        if not self.__launcher.getlaunchball().ismoving():
            self.__win.quit()
            quit()


base = Tk()
app = App(base)

base.mainloop()
