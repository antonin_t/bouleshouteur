class Border:
    """
    Classe 'Border' :
        Permet de stocker les quatre paramètres de bordure
        (minimum sur X, minimum sur Y, maximum sur X et maximum sur Y)
    """

    def __init__(self, minx, miny, maxx, maxy):
        self.__minx = minx
        self.__miny = miny
        self.__maxx = maxx
        self.__maxy = maxy

    def getminx(self):
        """
        :return: Min X
        """
        return self.__minx

    def getminy(self):
        """
        :return: Min Y
        """
        return self.__miny

    def getmaxx(self):
        """
        :return: Max X
        """
        return self.__maxx

    def getmaxy(self):
        """
        :return: Max Y
        """
        return self.__maxy

